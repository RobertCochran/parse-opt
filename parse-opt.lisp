(defpackage :parse-opt
  (:use :common-lisp)
  (:export :parse-opt))

(in-package :parse-opt)

(defun optionp (option)
  "Returns T if OPTION is an option (starts with a '-')."
  (char= (aref option 0) #\-))

(defun long-option-p (option)
  "Returns non NIL if OPTION is a long option (starts with '--')."
  (and (optionp option)
       (char= (aref option 1) #\-)))

(defun option-single-arg-p (option)
  "Returns non NIL if OPTION has an argument in single arg form (-farg or --foo=arg)"
  (if (long-option-p option)
      (position #\= option)
      (< 2 (length option))))

(defun get-option-string (option-string)
  "Get the string of an option given alone (-f or --foo)."
  (string (subseq option-string
		  (if (long-option-p option-string)
		      2
		      1))))

(defun split-option (option-string)
  "Split OPTION-STRING into option and argument."
  (let (option argument)
    (if (long-option-p option-string)
	(setq option
	      (subseq option-string
		      2 (position #\= option-string))
	      argument
	      (subseq option-string
		      (1+ (position #\= option-string))))
	(setq option
	      (aref option-string 1)
	      argument
	      (subseq option-string 2)))
    (mapcar #'string (list option argument))))

(defun get-option (option next-option option-specs)
  "Make an option pair from OPTION and NEXT-OPTION.

The car of the pair will be NIL if OPTION is not in OPTION-SPECS."
  (let ((find-fun (if (long-option-p option) #'second #'first))
	option-name
	option-arg)
    (if (option-single-arg-p option)
	(destructuring-bind (name arg) (split-option option)
	  (setq option-name name
		option-arg arg))
	(setq option-name (get-option-string option)))
    (let ((option-spec (find option-name option-specs :key find-fun :test #'equal)))
      (if option-spec
	  (destructuring-bind (short long arg-needed &optional option-value) option-spec
	    ; Set name to 'best' choice available
	    (setq option-name (or option-value long short))
	    (ecase arg-needed
	      (:none
	       ; We're done here
	       (cons option-name option-arg))
	      ((:optional :required)
	       (unless option-arg
		 ; Need see if NEXT-OPTION is our argument or not
		 (if (optionp next-option)
		     ; Nope! If we marked an arg as :REQUIRED, signal an error
		     (if (eq arg-needed :required)
			 (error "Missing required argument for ~s~%" option-name))
		     ; Assume it's our argument
		     (setq option-arg next-option)))
	       (cons option-name option-arg))))
	  (cons nil option-arg)))))

(defun parse-opt (option-list option-specs)
  "Parse OPTION-LIST according to OPTION-SPECS.

OPTION-LIST is a list of string options, presumably the command line arguments
from the invoking shell that have been space-split and made available by the
implementation.

OPTION-SPECS is a list of option specifications in the following format:

    (SHORT LONG ARGUMENT-NEEDED OPTION-VALUE)

SHORT is the single character form, given on the command line with a single
dash. It must be a single character string; a lone character will not work. This
may be NIL if there is no short variant of the option. The following examples
are considered valid short option specifications:

    -s     ; Short option 's' with no argument
    -sfoo  ; Short option 's' with argument 'foo', single argument
    -s foo ; Short option 's' with argument 'foo', separate arguments

LONG is the multiple character form, given on the command line with two dashes.
This may be NIL if there is no long variant of the option. The following
examples are considered valid short option specifications:

    --frob       ; Long option 'frob' with no argument
    --frob=extra ; Long option 'frob' with argument 'extra', single argument
    --frob extra ; Long option 'frob' with argument 'extra', separate arguments

ARGUMENT-NEEDED is one of :NONE, :OPTIONAL, or :REQUIRED.

OPTION-VALUE, if specified, is what will appear in the RECOGNIZED-OPTIONS alist
as the the car (identifier) of the option.

When determining what to use as the the option identifier, the following values
are searched in this order, using what is found first:

    OPTION-VALUE
    LONG
    SHORT

PARSE-OPTS returns a list with the following elements:

    (RECOGNIZED-OPTIONS LOOSE-ARGS UNRECOGNIZED-OPTIONS)

RECOGNIZED-OPTIONS is an alist of the options matched from OPTION-SPECS. If an
option does not have an argument, then the cdr of the cell is NIL.

LOOSE-ARGS are any non-option arguments not attached to an option.

UNRECOGNIZED-OPTIONS are options given that are not matched from OPTION-SPECS.
Arguments to unrecognized options given in separate argument form are considered
loose."
  (let (recognized-options loose-args unrecognized-options)
    (dotimes (i (length option-list))
      (let* ((current-option (nth i option-list))
	     (next-option (nth (1+ i) option-list)))
	(if (optionp current-option)
	    (let ((parsed-arg
		   (get-option current-option next-option option-specs)))
	      (if (car parsed-arg)
		  ; Recognized option
		  (progn
		    (push parsed-arg recognized-options)
		    ; Skip parsing next arg if it is our option arg
		    (if (string= (cdr parsed-arg)
				 next-option)
			(incf i)))
		  ; Unrecognized option
		  (push (cons (if (option-single-arg-p current-option)
				  (first (split-option current-option))
				  (get-option-string current-option))
			      (cdr parsed-arg))
			unrecognized-options)))
	    (push current-option loose-args))))
    (mapcar #'nreverse
	    (list recognized-options loose-args unrecognized-options))))
