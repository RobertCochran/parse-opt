(defpackage :parse-opt-asd
  (:use :common-lisp :asdf))

(in-package :parse-opt-asd)

(defsystem parse-opt
  :name "parse-opt"
  :version "1.0.0"
  :author "Robert Cochran"
  :license "GNU LGPLv3 (See LICENSE file)"
  :description "Better argument parsing facility"
  :components ((:file "parse-opt")))
